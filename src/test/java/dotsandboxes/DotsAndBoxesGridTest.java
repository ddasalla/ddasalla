package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIXME: You need to write tests for the two known bugs in the code.
    @Test
    public void testDrawLineAlreadyDrawn() {
        logger.info("Testing line that has already been drawn");

        // Draw a line
        game.drawHorizontal(0, 0, 1);

        // Try to draw the same line again
        Exception exception = assertThrows(IllegalStateException.class, () -> {
            game.drawHorizontal(0, 0, 1);
        });

        assertEquals("The horizontal line at (0, 0) is already drawn.", exception.getMessage());
    }

    @Test
    public void testSquareCompletion() {
        logger.info("Testing square completion");

        // Draw lines to form a square
        game.drawHorizontal(0, 0, 1);
        game.drawVertical(0, 0, 1);
        game.drawHorizontal(0, 1, 1);
        game.drawVertical(1, 0, 1);

        // Try to draw the last line to complete the square
        boolean result = game.drawHorizontal(1, 0, 1);

        // Verify the result
        
        assertTrue(game.boxComplete(0, 0), "The box at (0, 0) should be complete.");
        game = new DotsAndBoxesGrid(3, 3);
        game.drawHorizontal(0, 0, 1);
        game.drawVertical(0, 0, 1);
        assertFalse(game.boxComplete(0, 0), "The box at (0, 0) should not be complete.");
    }
}


